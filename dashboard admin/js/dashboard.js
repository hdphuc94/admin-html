(function($) {
    var checkAllList = 0,
        dataCheckInput = [],
        formCheckAll =  $('.form-check-all'),
        itemCheckInput = $('.item-check-input'),
        btnAddShow = $('.btn-add-customer'),
        btnRemoveShow = $('.btn-remove-custom');
    $(document).ready(function() {
        // for event click dropdown bootstrap
        $('.dropdown-toggle').dropdown();
        // Menu show hide
        $('.has-sub > a').click(function() {
            $(this).toggleClass('opensub').siblings('.menu-content').slideToggle(300);
        });

        //select all list
        formCheckAll.change(function() {
            var checkAll = 1;
            if (!$(this).is(':checked')) {
                checkAll = 0;
                btnAddShow.show();
                btnRemoveShow.hide();
            }else{
                dataCheckInput = [];
                btnAddShow.hide();
                btnRemoveShow.show();
            }
            itemCheckInput.each(function(index, val) {
                if (checkAll) {
                    if(!$(this).is(':checked')){
                        $(this).trigger("click");
                    }
                } else {
                    if($(this).is(':checked')){
                        $(this).trigger("click");
                    }
                }
            });
        });        
        // select checkbox show delete button
        itemCheckInput.change(function(e) {
            // reset checkAllList
            $numberItemCheckInput = itemCheckInput.length;
            if(checkAllList < 0 || parseInt($numberItemCheckInput) < checkAllList ){
                checkAllList = 0;
            }
            if (!$(this).is(':checked')){
                dataCheckInput.splice($(this).closest('tr').index(), 1);
                checkAllList--;
            }else{
                dataCheckInput[$(this).closest('tr').index()] = jQuery(this).val();
                checkAllList++;
            }
            if(checkAllList < 0){
                checkAllList = 0;
            }
            // get data when checkbox select item
            if (checkAllList != 0 ) {
                btnAddShow.hide();
                btnRemoveShow.show();
            } else if(checkAllList == 0){
                if(formCheckAll.is(':checked')){
                    $('.form-check-all:checked').trigger("click");
                }
                btnAddShow.show();
                btnRemoveShow.hide();
            }
        });
        // event with data input
        btnRemoveShow.click(function(){
            // console.log(dataCheckInput);
        });

        // Event click action
        $('.item-actions .action').click(function(event) {
            if (!$(this).is('.show')) {
                $('.actions-content').hide();
                $('.item-actions .action').removeClass('show');
            }
            $(this).toggleClass('show').siblings('.actions-content').slideToggle(400);
        });

        // Show hide menu in mobile
        $('.btn-menu-toggle').click(function(e){
            $(this).find('i').toggleClass('fa-ellipsis-v fa-times');
            $(this).closest('.main-menu-content').toggleClass('show-mobile');
        });
    });
})(jQuery)